package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/bugsnag/bugsnag-go"
	"gitlab.com/gustavodebiasi/b2w_go/config"
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gitlab.com/gustavodebiasi/b2w_go/models"
)

type PlanetaSwapi struct {
	Name  string   `json:"name"`
	Films []string `json:"films"`
}

type SwapiGetPlanetas struct {
	Count    int            `json:"count"`
	Next     string         `json:"next"`
	Previous string         `json:"previous"`
	Results  []PlanetaSwapi `json:"results"`
}

func main() {
	bugsnag.Configure(bugsnag.Configuration{
		APIKey:          "6a6ecef542395765f1ade5338e8bb0fd",
		ProjectPackages: []string{"main", "gitlab.com/gutavodebiasi/b2w_go"},
	})

	config.CarregarEnv(false)
	var db = database.MongoConnection{}
	db.Conectar()
	swapiRepository := models.NovoSwapiRepository(db)

	AtualizarRegistrosSwapi(swapiRepository)
}

func AtualizarRegistrosSwapi(swapiRepository models.ISwapiRepository) {
	fmt.Println("Iniciando processo de atualização do swapi")
	urlRequest := "https://swapi.dev/api/planets"

	for {
		swapiPlanetas, erro := requisicaoSwapi(urlRequest)
		if erro != nil {
			bugsnag.Notify(erro)
			break
		}

		quantidadeResultados := len(swapiPlanetas.Results)
		if quantidadeResultados > 0 {
			urlRequest = swapiPlanetas.Next

			for i := 0; i < quantidadeResultados; i++ {
				var swapi models.Swapi
				registro := swapiPlanetas.Results[i]
				swapi.Nome = registro.Name
				swapi.QuantidadeFilmes = int32(len(registro.Films))

				erro := swapiRepository.CriarOuAtualizar(swapi)
				if erro != nil {
					bugsnag.Notify(erro)
					break
				}
			}
		}

		if swapiPlanetas.Next == "" || quantidadeResultados == 0 {
			break
		}
	}
	fmt.Println("Processo de atualização do swapi terminado!")
}

func requisicaoSwapi(urlRequest string) (SwapiGetPlanetas, error) {
	var swapiPlanetas SwapiGetPlanetas

	request, erro := http.NewRequest(http.MethodGet, urlRequest, nil)
	if erro != nil {
		swapiPlanetas = SwapiGetPlanetas{
			Next:  urlRequest,
			Count: 0,
		}
		return swapiPlanetas, nil
	}
	var client = &http.Client{}
	response, erro := client.Do(request)
	if erro != nil {
		return swapiPlanetas, erro
	}

	if erro := json.NewDecoder(response.Body).Decode(&swapiPlanetas); erro != nil {
		return swapiPlanetas, erro
	}

	return swapiPlanetas, nil
}
