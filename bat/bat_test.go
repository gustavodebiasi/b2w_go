package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/models"
	"gitlab.com/gustavodebiasi/b2w_go/tests"
	"gopkg.in/mgo.v2/bson"
)

type SwapiRepositoryMock struct{}

func (srm *SwapiRepositoryMock) CriarOuAtualizar(swapi models.Swapi) error {
	return errors.New("mensagem com debug")
}

func (srm *SwapiRepositoryMock) BuscarPorNome(nome string) (models.Swapi, error) {
	return models.Swapi{}, errors.New("mensagem com debug")
}

func TestAtualizarRegistrosSwapi(t *testing.T) {
	t.Run("atualização de registro com sucesso", func(t *testing.T) {
		db := tests.InicializarTestes()
		swapiRepository := models.NovoSwapiRepository(db)

		AtualizarRegistrosSwapi(swapiRepository)

		collection := db.Collection("swapi")
		quantidade, _ := collection.Find(bson.M{}).Count()
		assert.Equal(t, 60, quantidade)
	})

	t.Run("atualização de registro com erro", func(t *testing.T) {
		db := tests.InicializarTestes()
		swapiRepository := SwapiRepositoryMock{}

		AtualizarRegistrosSwapi(&swapiRepository)

		collection := db.Collection("swapi")
		quantidade, _ := collection.Find(bson.M{}).Count()
		assert.Equal(t, 0, quantidade)
	})
}
