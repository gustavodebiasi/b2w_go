package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/gustavodebiasi/b2w_go/api"
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gitlab.com/gustavodebiasi/b2w_go/models"
)

func ConfigurarRotas(db database.MongoConnection) *mux.Router {
	router := mux.NewRouter()
	p := router.PathPrefix("/planetas").Subrouter()
	planetaRepository := models.NovoPlanetaRepository(db)
	swapiRepository := models.NovoSwapiRepository(db)
	p.HandleFunc("", api.CriarPlanetaHandler(planetaRepository, swapiRepository)).Methods("POST")
	p.HandleFunc("", api.BuscarPlanetaHandler(planetaRepository)).Methods("GET")
	p.HandleFunc("/{id}", api.ExcluirPlanetaHandler(planetaRepository)).Methods("DELETE")

	return router
}
