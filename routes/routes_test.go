package routes

import (
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/config"
	"gitlab.com/gustavodebiasi/b2w_go/database"
)

func TestConfigurarRotas(t *testing.T) {
	config.CarregarEnv(true)
	db := database.MongoConnection{}
	db.Conectar()
	router := ConfigurarRotas(db)

	var tipoEsperado *mux.Router
	assert.IsType(t, tipoEsperado, router)
}
