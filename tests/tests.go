package tests

import (
	"gitlab.com/gustavodebiasi/b2w_go/config"
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gopkg.in/mgo.v2"
)

func limparCollection(c *mgo.Collection) {
	c.RemoveAll(nil)
}

func InicializarTestes() database.MongoConnection {
	config.CarregarEnv(true)
	var db = database.MongoConnection{}
	db.Conectar()
	limparCollection(db.Collection("swapi"))
	limparCollection(db.Collection("planetas"))

	return db
}
