[![pipeline status](https://gitlab.com/gustavodebiasi/b2w_go/badges/master/pipeline.svg)](https://gitlab.com/gustavodebiasi/b2w_go/-/commits/master)

[![coverage report](https://gitlab.com/gustavodebiasi/b2w_go/badges/master/coverage.svg)](https://gitlab.com/gustavodebiasi/b2w_go/-/commits/master)

## API Star Wars

Essa API foi criada como forma de validação técnica para processo seletivo na empresa B2W.

## Pré-requesitos

- Docker

## Configuração

- Configure o seu .env (no repositório há um .env.default para você se familiarizar com as variáveis necessárias)

## Execução do projeto

- Faça download do projeto e coloque no seu GOPATH
- Para o projeto é necessário ter um banco de dados mongoDB, então você pode configurar o seu banco de dados ou utilizar o docker para isso:
```
docker-compose up -d database
```
- Para rodar o comando de cache do Swapi, execute o bat/bat.go ou utilize o docker para isso:
```
docker-compose up -d bat
```
- Para rodar o projeto, execute o main.go na raiz do projeto ou utilize o docker para isso:
```
docker-compose up -d api
```

## Utilização

No arquivo API Star Wars.postman_collection.json há uma coleção do postman para realizar a utilização da API. De igual forma, os endpoints são descritos abaixo.

- **GET** - **/planetas**: Retornando a lista de planetas disponíveis e seus atributos

- **GET** - **/planetas?id=:id**: Utilizando o query string com o id como parâmetro a API irá buscar e retornar o planeta referente ao ID.

- **GET** - **/planetas?nome=:nome**: Utilizando o query string com o nome do planeta como parâmetro a API irá buscar e retornar o(s) planeta(s) com o nome pesquisado.

- **POST** - **/planetas**: Utilize este endpoint para criar um novo planeta. Neste endpoint é obrigatório o envio das seguintes informações no corpo da requisição: nome, terreno e clima.

- **DELETE** - **/planetas/:id**: Para deletar um planeta. Neste endpoint é obrigatório o envio do ID na requisição.


## Desenvolvimento

- Você pode utilizar o air para hot reload (para instalar e configurar utilize a documentação oficial, disponível em: https://github.com/cosmtrek/air).

## Testes Unitários

- Para rodar os testes, os mesmos são executados utilizando o banco de dados configurado no .env, então, primeiro configure o seu .env e após execute o comando:

```
make test
```
