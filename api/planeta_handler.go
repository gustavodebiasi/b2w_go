package api

import (
	"encoding/json"
	"net/http"

	"github.com/asaskevich/govalidator"
	"github.com/bugsnag/bugsnag-go"
	"gitlab.com/gustavodebiasi/b2w_go/models"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func CriarPlanetaHandler(planetaRepository models.IPlanetaRepository, swapiRepository models.ISwapiRepository) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		var planeta models.Planeta
		json.NewDecoder(r.Body).Decode(&planeta)
		_, erro := govalidator.ValidateStruct(planeta)
		if erro != nil {
			retornoHTTPJson(w, govalidator.ErrorsByField(erro), http.StatusUnprocessableEntity)
			return
		}

		swapi, erro := swapiRepository.BuscarPorNome(planeta.Nome)
		var quantidadeFilmesSwapi int32 = 0
		if erro != mgo.ErrNotFound {
			quantidadeFilmesSwapi = swapi.QuantidadeFilmes
		}
		if erro != nil && erro != mgo.ErrNotFound {
			bugsnag.Notify(erro, r.Context())
			retornoErroInternoJson(w, erro.Error())
			return
		}
		planeta.QuantidadeFilmes = quantidadeFilmesSwapi

		planeta, erro = planetaRepository.Criar(planeta)
		if erro != nil {
			bugsnag.Notify(erro, r.Context())
			retornoErroInternoJson(w, erro.Error())
			return
		}

		retornoHTTPJson(w, planeta, http.StatusCreated)
		return
	}
}

func buscarPlanetaPorId(planetaRepository models.IPlanetaRepository, id string, w http.ResponseWriter) {
	if !bson.IsObjectIdHex(id) {
		retorno := map[string]string{"id": "ID inválido"}
		retornoHTTPJson(w, retorno, http.StatusUnprocessableEntity)
		return
	}
	objectId := bson.ObjectIdHex(id)

	planeta, erro := planetaRepository.BuscarPorId(objectId)
	if erro == mgo.ErrNotFound {
		retornoNaoEncontradoJson(w)
		return
	}
	if erro != nil {
		bugsnag.Notify(erro)
		retornoErroInternoJson(w, erro.Error())
		return
	}

	retornoHTTPJson(w, planeta, http.StatusOK)
	return
}

func buscarPlanetaPorNome(planetaRepository models.IPlanetaRepository, nome string, w http.ResponseWriter) {
	var planetas []models.Planeta = make([]models.Planeta, 0)
	planetas, erro := planetaRepository.BuscarPorNome(nome)
	if erro != nil {
		bugsnag.Notify(erro)
		retornoErroInternoJson(w, erro.Error())
		return
	}

	retornoHTTPJson(w, planetas, http.StatusOK)
	return
}

func BuscarPlanetaHandler(planetaRepository models.IPlanetaRepository) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		if query["id"] != nil {
			id := query["id"][0]
			buscarPlanetaPorId(planetaRepository, id, w)
			return
		}
		if query["nome"] != nil {
			nome := query["nome"][0]
			buscarPlanetaPorNome(planetaRepository, nome, w)
			return
		}

		planetas, erro := planetaRepository.BuscarTodos()
		if erro != nil {
			bugsnag.Notify(erro, r.Context())
			retornoErroInternoJson(w, erro.Error())
			return
		}

		retornoHTTPJson(w, planetas, http.StatusOK)
		return
	}
}

func ExcluirPlanetaHandler(planetaRepository models.IPlanetaRepository) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		tamanhoUrl := len("/planetas/")
		if len(r.URL.Path) <= tamanhoUrl {
			retornoNaoEncontradoJson(w)

			return
		}
		id := r.URL.Path[len("/planetas/"):]

		if !bson.IsObjectIdHex(id) {
			retorno := map[string]string{"id": "ID inválido"}
			retornoHTTPJson(w, retorno, http.StatusUnprocessableEntity)
			return
		}
		objectId := bson.ObjectIdHex(id)
		erro := planetaRepository.Excluir(objectId)

		if erro == mgo.ErrNotFound {
			retornoNaoEncontradoJson(w)
			return
		}
		if erro != nil {
			bugsnag.Notify(erro, r.Context())
			retornoErroInternoJson(w, erro.Error())
			return
		}

		retorno := map[string]string{"status": "Planeta excluído com sucesso!"}
		retornoHTTPJson(w, retorno, http.StatusOK)

		return
	}
}
