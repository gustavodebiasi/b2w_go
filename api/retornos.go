package api

import (
	"encoding/json"
	"net/http"
	"os"
)

func retornoFalha(w http.ResponseWriter, mensagem string, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	texto := []byte(mensagem)
	w.Write(texto)
}

func retornoErroInternoJson(w http.ResponseWriter, mensagem string) {
	if os.Getenv("DEBUG") == "TRUE" {
		retornoFalha(w, mensagem, http.StatusInternalServerError)
	} else {
		retornoFalha(w, "Ocorreu um erro interno", http.StatusInternalServerError)
	}
}

func retornoNaoEncontradoJson(w http.ResponseWriter) {
	retornoFalha(w, "Registro não encontrado", http.StatusNotFound)
}

func retornoHTTPJson(w http.ResponseWriter, data interface{}, status int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if data != nil {
		json.NewEncoder(w).Encode(data)
	}
}
