package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/models"
	"gitlab.com/gustavodebiasi/b2w_go/tests"
	"gopkg.in/mgo.v2/bson"
)

type PlanetaRepositoryMock struct{}

func (prm *PlanetaRepositoryMock) Criar(p models.Planeta) (models.Planeta, error) {
	return models.Planeta{}, errors.New("mensagem com debug")
}

func (prm *PlanetaRepositoryMock) BuscarPorId(id bson.ObjectId) (models.Planeta, error) {
	return models.Planeta{}, errors.New("mensagem com debug")
}

func (prm *PlanetaRepositoryMock) BuscarPorNome(nome string) ([]models.Planeta, error) {
	var planetas []models.Planeta
	return planetas, errors.New("mensagem com debug")
}

func (prm *PlanetaRepositoryMock) BuscarTodos() ([]models.Planeta, error) {
	var planetas []models.Planeta
	return planetas, errors.New("mensagem com debug")
}

func (prm *PlanetaRepositoryMock) Excluir(id bson.ObjectId) error {
	return errors.New("mensagem com debug")
}

type SwapiRepositoryMock struct{}

func (srm *SwapiRepositoryMock) CriarOuAtualizar(swapi models.Swapi) error {
	return errors.New("mensagem com debug")
}

func (srm *SwapiRepositoryMock) BuscarPorNome(nome string) (models.Swapi, error) {
	return models.Swapi{}, errors.New("mensagem com debug")
}

func TestBuscarPlanetaHandler(t *testing.T) {
	t.Run("busca planetas com erro sem debug", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/planetas", nil)
		response := httptest.NewRecorder()

		planetaRepository := PlanetaRepositoryMock{}

		(BuscarPlanetaHandler(&planetaRepository)(response, request))
		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "Ocorreu um erro interno", bodyRecebido)
	})

	t.Run("busca planeta por id com erro", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/planetas?id=5f3d2fdce0a513019804a601", nil)
		response := httptest.NewRecorder()

		planetaRepository := PlanetaRepositoryMock{}

		(BuscarPlanetaHandler(&planetaRepository)(response, request))
		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "Ocorreu um erro interno", bodyRecebido)
	})

	t.Run("busca planeta com ID inválido", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodGet, "/planetas?id=2", nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusUnprocessableEntity, statusRecebido)
		assert.JSONEq(t, `{"id": "ID inválido"}`, bodyRecebido)
	})

	t.Run("busca planeta com ID válido mas inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodGet, "/planetas?id=5f3d2fdce0a513019804a601", nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusNotFound, statusRecebido)
		assert.Equal(t, "Registro não encontrado", bodyRecebido)
	})

	t.Run("busca planeta com ID", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planeta := models.Planeta{
			Nome:    "Teste com busca com ID",
			Clima:   "Seco",
			Terreno: "Plano",
		}

		response := CriarPlaneta(planetaRepository, swapiRepository, planeta)

		var planetaCadastrado models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetaCadastrado)

		id := bson.ObjectId(planetaCadastrado.Id).Hex()
		request, _ := http.NewRequest(http.MethodGet, "/planetas?id="+id, nil)
		response = httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)

		var planetaRecebido models.Planeta
		bodyRecebido = response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetaRecebido)

		assert.Equal(t, planetaCadastrado, planetaRecebido)
	})

	t.Run("busca planeta com nome mas inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodGet, "/planetas?nome=teste", nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)
		assert.Equal(t, "[]\n", bodyRecebido)
	})

	t.Run("busca planeta com nome com erro", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/planetas?nome=teste", nil)
		response := httptest.NewRecorder()

		planetaRepository := PlanetaRepositoryMock{}

		(BuscarPlanetaHandler(&planetaRepository)(response, request))
		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "mensagem com debug", bodyRecebido)
	})

	t.Run("busca planeta com nome", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planeta := models.Planeta{
			Nome:    "Teste com busca com ID",
			Clima:   "Seco",
			Terreno: "Plano",
		}

		CriarPlaneta(planetaRepository, swapiRepository, planeta)

		nome := url.QueryEscape(planeta.Nome)
		request, _ := http.NewRequest(http.MethodGet, "/planetas?nome="+nome, nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)

		var planetasRecebidos []models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetasRecebidos)

		assert.NotEmpty(t, planetasRecebidos[0])
		planetasRecebidos[0].Id = ""
		assert.Equal(t, planeta, planetasRecebidos[0])
	})

	t.Run("busca planetas com retorno vazio", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodGet, "/planetas", nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)
		assert.Equal(t, "[]\n", bodyRecebido)
	})

	t.Run("busca planetas com 2 planetas", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planetaUm := models.Planeta{
			Nome:    "Tootine",
			Clima:   "verão",
			Terreno: "plano",
		}

		planetaDois := models.Planeta{
			Nome:    "Terra",
			Clima:   "Inverno",
			Terreno: "Montanhoso",
		}

		CriarPlaneta(planetaRepository, swapiRepository, planetaUm)
		CriarPlaneta(planetaRepository, swapiRepository, planetaDois)

		request, _ := http.NewRequest(http.MethodGet, "/planetas", nil)
		response := httptest.NewRecorder()

		(BuscarPlanetaHandler(planetaRepository)(response, request))

		var planetasRecebidos []models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetasRecebidos)

		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)
		assert.NotEmpty(t, planetasRecebidos[0])
		assert.NotEmpty(t, planetasRecebidos[1])
		planetasRecebidos[0].Id = ""
		planetasRecebidos[1].Id = ""
		assert.Equal(t, planetaUm, planetasRecebidos[0])
		assert.Equal(t, planetaDois, planetasRecebidos[1])
	})
}

func TestCriarPlanetaHandler(t *testing.T) {
	t.Run("criar planeta com erro", func(t *testing.T) {
		db := tests.InicializarTestes()
		swapiRepository := models.NovoSwapiRepository(db)

		planetaRepository := PlanetaRepositoryMock{}

		planetaEnviado := models.Planeta{
			Nome:    "Teste",
			Clima:   "Verão",
			Terreno: "Planície",
		}
		response := CriarPlaneta(&planetaRepository, swapiRepository, planetaEnviado)

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "mensagem com debug", bodyRecebido)
	})

	t.Run("Criar planeta sem dados", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		var jsonPost = []byte(`{}`)
		request, _ := http.NewRequest(http.MethodPost, "/planetas", bytes.NewBuffer(jsonPost))
		response := httptest.NewRecorder()

		(CriarPlanetaHandler(planetaRepository, swapiRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		bodyEsperado := "{\"clima\":\"non zero value required\",\"nome\":\"non zero value required\",\"terreno\":\"non zero value required\"}\n"
		assert.Equal(t, http.StatusUnprocessableEntity, statusRecebido)
		assert.Equal(t, bodyEsperado, bodyRecebido)
	})

	t.Run("Criar planeta com falha de tamanho de string", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planetaEnviado := models.Planeta{
			Nome:    "a",
			Clima:   "b",
			Terreno: "c",
		}

		jsonPost, _ := json.Marshal(&planetaEnviado)
		jsonPost = []byte(jsonPost)
		request, _ := http.NewRequest(http.MethodPost, "/planetas", bytes.NewBuffer(jsonPost))
		response := httptest.NewRecorder()

		(CriarPlanetaHandler(planetaRepository, swapiRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code

		bodyEsperado := "{\"clima\":\"b does not validate as stringlength(3|100)\",\"nome\":\"a does not validate as stringlength(3|100)\",\"terreno\":\"c does not validate as stringlength(3|100)\"}\n"
		assert.Equal(t, http.StatusUnprocessableEntity, statusRecebido)
		assert.Equal(t, bodyEsperado, bodyRecebido)
	})

	t.Run("Criar planeta sem ter Swapi", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planetaEnviado := models.Planeta{
			Nome:    "Teste",
			Clima:   "Verão",
			Terreno: "Planície",
		}

		response := CriarPlaneta(planetaRepository, swapiRepository, planetaEnviado)

		var planetaRecebido models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetaRecebido)
		statusRecebido := response.Code
		assert.Equal(t, http.StatusCreated, statusRecebido)
		assert.NotEmpty(t, planetaRecebido.Id)
		planetaRecebido.Id = ""
		assert.Equal(t, planetaEnviado, planetaRecebido)
	})

	t.Run("Criar planeta com Swapi", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		swapi := models.Swapi{Nome: "Coruscant", QuantidadeFilmes: 4}
		swapiRepository.CriarOuAtualizar(swapi)

		planetaEnviado := models.Planeta{
			Nome:    "Coruscant",
			Clima:   "Verão",
			Terreno: "Planície",
		}

		response := CriarPlaneta(planetaRepository, swapiRepository, planetaEnviado)

		var planetaRecebido models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetaRecebido)
		statusRecebido := response.Code
		assert.Equal(t, http.StatusCreated, statusRecebido)
		assert.NotEmpty(t, planetaRecebido.Id)
		planetaRecebido.Id = ""
		planetaEnviado.QuantidadeFilmes = 4
		assert.Equal(t, planetaEnviado, planetaRecebido)
	})

	t.Run("Criar planeta com Swapi e erro", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := SwapiRepositoryMock{}

		swapi := models.Swapi{Nome: "Coruscant", QuantidadeFilmes: 4}
		swapiRepository.CriarOuAtualizar(swapi)

		planetaEnviado := models.Planeta{
			Nome:    "Coruscant",
			Clima:   "Verão",
			Terreno: "Planície",
		}

		response := CriarPlaneta(planetaRepository, &swapiRepository, planetaEnviado)

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "mensagem com debug", bodyRecebido)
	})
}

func TestExcluirPlanetaHandler(t *testing.T) {
	t.Run("excluir planeta com erro", func(t *testing.T) {
		request, _ := http.NewRequest(http.MethodGet, "/planetas/5f3d2fdce0a513019804a601", nil)
		response := httptest.NewRecorder()

		planetaRepository := PlanetaRepositoryMock{}

		(ExcluirPlanetaHandler(&planetaRepository)(response, request))
		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusInternalServerError, statusRecebido)
		assert.Equal(t, "mensagem com debug", bodyRecebido)
	})

	t.Run("excluir planeta com ID inválido", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodDelete, "/planetas/2", nil)
		response := httptest.NewRecorder()

		(ExcluirPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusUnprocessableEntity, statusRecebido)
		assert.JSONEq(t, `{"id": "ID inválido"}`, bodyRecebido)
	})

	t.Run("excluir planeta sem informar ID", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodDelete, "/planetas/", nil)
		response := httptest.NewRecorder()

		(ExcluirPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusNotFound, statusRecebido)
		assert.Equal(t, "Registro não encontrado", bodyRecebido)
	})

	t.Run("excluir planeta com ID válido mas inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)

		request, _ := http.NewRequest(http.MethodDelete, "/planetas/5f3d2fdce0a513019804a601", nil)
		response := httptest.NewRecorder()

		(ExcluirPlanetaHandler(planetaRepository)(response, request))

		bodyRecebido := response.Body.String()
		statusRecebido := response.Code
		assert.Equal(t, http.StatusNotFound, statusRecebido)
		assert.Equal(t, "Registro não encontrado", bodyRecebido)
	})

	t.Run("excluir planeta", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := models.NovoPlanetaRepository(db)
		swapiRepository := models.NovoSwapiRepository(db)

		planeta := models.Planeta{
			Nome:    "Teste com busca com ID",
			Clima:   "Seco",
			Terreno: "Plano",
		}

		response := CriarPlaneta(planetaRepository, swapiRepository, planeta)

		var planetaCadastrado models.Planeta
		bodyRecebido := response.Body.String()
		json.Unmarshal([]byte(bodyRecebido), &planetaCadastrado)

		id := bson.ObjectId(planetaCadastrado.Id).Hex()
		request, _ := http.NewRequest(http.MethodDelete, "/planetas/"+id, nil)
		response = httptest.NewRecorder()

		(ExcluirPlanetaHandler(planetaRepository)(response, request))

		statusRecebido := response.Code
		assert.Equal(t, http.StatusOK, statusRecebido)
		bodyRecebido = response.Body.String()
		assert.JSONEq(t, `{"status": "Planeta excluído com sucesso!"}`, bodyRecebido)
	})
}

func CriarPlaneta(planetaRepository models.IPlanetaRepository, swapiRepository models.ISwapiRepository, p models.Planeta) *httptest.ResponseRecorder {
	jsonPost, _ := json.Marshal(p)
	jsonPost = []byte(jsonPost)
	request, _ := http.NewRequest(http.MethodPost, "/planetas", bytes.NewBuffer(jsonPost))
	response := httptest.NewRecorder()

	(CriarPlanetaHandler(planetaRepository, swapiRepository)(response, request))
	return response
}
