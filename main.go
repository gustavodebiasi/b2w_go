package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/bugsnag/bugsnag-go"
	"gitlab.com/gustavodebiasi/b2w_go/config"
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gitlab.com/gustavodebiasi/b2w_go/routes"
)

func init() {
	config.CarregarEnv(false)
}

func main() {
	bugsnag.Configure(bugsnag.Configuration{
		APIKey:          "6a6ecef542395765f1ade5338e8bb0fd",
		ProjectPackages: []string{"main", "gitlab.com/gutavodebiasi/b2w_go"},
	})

	var db = database.MongoConnection{}
	db.Conectar()
	router := routes.ConfigurarRotas(db)
	porta := fmt.Sprintf(":%s", os.Getenv("API_PORT"))

	fmt.Println("Listening on localhost" + porta)
	http.ListenAndServe(porta, router)
}
