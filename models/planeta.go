package models

import (
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Planeta struct {
	Id               bson.ObjectId `json:"id" bson:"_id"`
	Nome             string        `json:"nome" bson:"nome" valid:"required,stringlength(3|100)"`
	Terreno          string        `json:"terreno" bson:"terreno" valid:"required,stringlength(3|100)"`
	Clima            string        `json:"clima" bson:"clima" valid:"required,stringlength(3|100)"`
	QuantidadeFilmes int32         `json:"quantidade_filmes" bson:"quantidade_filmes"`
}

type PlanetaRepository struct {
	database   database.MongoConnection
	collection *mgo.Collection
}

type IPlanetaRepository interface {
	Criar(Planeta) (Planeta, error)
	BuscarPorId(bson.ObjectId) (Planeta, error)
	BuscarPorNome(string) ([]Planeta, error)
	BuscarTodos() ([]Planeta, error)
	Excluir(bson.ObjectId) error
}

func NovoPlanetaRepository(d database.MongoConnection) *PlanetaRepository {
	return &PlanetaRepository{
		database:   d,
		collection: d.Collection("planetas"),
	}
}

func (p *PlanetaRepository) Criar(planeta Planeta) (Planeta, error) {
	if planeta.Id == "" {
		planeta.Id = bson.NewObjectId()
	}
	erro := p.collection.Insert(planeta)

	return planeta, erro
}

func (p *PlanetaRepository) BuscarTodos() ([]Planeta, error) {
	var planetas []Planeta = make([]Planeta, 0)
	erro := p.collection.Find(nil).All(&planetas)

	return planetas, erro
}

func (p *PlanetaRepository) BuscarPorNome(nome string) ([]Planeta, error) {
	filter := bson.M{"nome": nome}

	var planetas []Planeta = make([]Planeta, 0)
	erro := p.collection.Find(filter).All(&planetas)

	return planetas, erro
}

func (p *PlanetaRepository) BuscarPorId(id bson.ObjectId) (Planeta, error) {
	var planeta Planeta
	erro := p.collection.FindId(id).One(&planeta)

	return planeta, erro
}

func (p *PlanetaRepository) Excluir(id bson.ObjectId) error {
	erro := p.collection.RemoveId(id)

	return erro
}
