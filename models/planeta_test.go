package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/tests"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func TestNovoPlanetaRepository(t *testing.T) {
	db := tests.InicializarTestes()
	planetaRepository := NovoPlanetaRepository(db)

	var tipoEsperado *PlanetaRepository
	assert.IsType(t, tipoEsperado, planetaRepository)
}

func TestCriar(t *testing.T) {
	db := tests.InicializarTestes()
	planetaRepository := NovoPlanetaRepository(db)

	planeta := Planeta{Nome: "teste", Clima: "teste", Terreno: "teste"}
	planetaCriado, erro := planetaRepository.Criar(planeta)

	assert.Equal(t, nil, erro)
	planetaCriado.Id = ""
	assert.Equal(t, planeta, planetaCriado)
}

func TestBuscarTodos(t *testing.T) {
	t.Run("buscar todos vazio", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetasBuscados, erro := planetaRepository.BuscarTodos()

		assert.Equal(t, nil, erro)
		assert.Empty(t, planetasBuscados)
	})

	t.Run("buscar todos", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetaCriado := Planeta{Nome: "obviato", Clima: "obviato", Terreno: "obviato"}
		var planetas []Planeta
		planetaRepository.Criar(planetaCriado)
		planetas = append(planetas, planetaCriado)

		planetasBuscados, erro := planetaRepository.BuscarTodos()

		assert.Equal(t, nil, erro)
		assert.NotEmpty(t, planetasBuscados[0].Id)
		planetasBuscados[0].Id = ""
		assert.Equal(t, planetas, planetasBuscados)
	})
}

func TestBuscarPorNomePlaneta(t *testing.T) {
	t.Run("Buscar por nome inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetas, erro := planetaRepository.BuscarPorNome("teste")

		assert.Equal(t, nil, erro)
		assert.Empty(t, planetas)
	})

	t.Run("Buscar por nome", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetaCriado := Planeta{Nome: "obviato", Clima: "obviato", Terreno: "obviato"}
		var planetas []Planeta
		planetaRepository.Criar(planetaCriado)
		planetas = append(planetas, planetaCriado)

		planetasBuscados, erro := planetaRepository.BuscarPorNome("obviato")

		assert.Equal(t, nil, erro)
		assert.NotEmpty(t, planetasBuscados[0].Id)
		planetasBuscados[0].Id = ""
		assert.Equal(t, planetas, planetasBuscados)
	})
}

func TestBuscarPorId(t *testing.T) {
	t.Run("Buscar por id inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planeta, erro := planetaRepository.BuscarPorId(bson.NewObjectId())

		assert.Equal(t, mgo.ErrNotFound, erro)
		assert.Empty(t, planeta)
	})

	t.Run("Buscar por ID existente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetaCriado := Planeta{
			Id:      bson.NewObjectId(),
			Nome:    "obviato",
			Clima:   "obviato",
			Terreno: "obviato",
		}
		planetaRepository.Criar(planetaCriado)

		planetaBuscado, erro := planetaRepository.BuscarPorId(planetaCriado.Id)

		assert.Equal(t, nil, erro)
		assert.Equal(t, planetaCriado, planetaBuscado)
	})
}

func TestExcluirPlaneta(t *testing.T) {
	t.Run("Excluir por id inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		erro := planetaRepository.Excluir(bson.NewObjectId())

		assert.Equal(t, mgo.ErrNotFound, erro)
	})

	t.Run("Excluir por ID existente", func(t *testing.T) {
		db := tests.InicializarTestes()
		planetaRepository := NovoPlanetaRepository(db)

		planetaCriado := Planeta{
			Id:      bson.NewObjectId(),
			Nome:    "obviato",
			Clima:   "obviato",
			Terreno: "obviato",
		}
		planetaRepository.Criar(planetaCriado)

		erro := planetaRepository.Excluir(planetaCriado.Id)

		assert.Equal(t, nil, erro)
	})
}
