package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/tests"
	"gopkg.in/mgo.v2"
)

func TestNovoSwapiRepository(t *testing.T) {
	db := tests.InicializarTestes()
	swapiRepository := NovoSwapiRepository(db)

	var tipoEsperado *SwapiRepository
	assert.IsType(t, tipoEsperado, swapiRepository)
}

func TestCriarOuAtualizar(t *testing.T) {
	db := tests.InicializarTestes()
	swapiRepository := NovoSwapiRepository(db)

	swapi := Swapi{Nome: "Coruscant", QuantidadeFilmes: 4}
	erro := swapiRepository.CriarOuAtualizar(swapi)
	assert.Equal(t, nil, erro)
}

func TestBuscarPorNomeSwapi(t *testing.T) {
	t.Run("nome inexistente", func(t *testing.T) {
		db := tests.InicializarTestes()
		swapiRepository := NovoSwapiRepository(db)

		swapi, erro := swapiRepository.BuscarPorNome("Coruscant")
		assert.Equal(t, mgo.ErrNotFound, erro)
		assert.Equal(t, Swapi{}, swapi)
	})

	t.Run("nome existente", func(t *testing.T) {
		db := tests.InicializarTestes()
		swapiRepository := NovoSwapiRepository(db)

		swapi := Swapi{Nome: "oi", QuantidadeFilmes: 5}
		swapiRepository.CriarOuAtualizar(swapi)

		swapiRecebido, erro := swapiRepository.BuscarPorNome("oi")
		swapiRecebido.Id = ""
		assert.Equal(t, nil, erro)
		assert.Equal(t, swapi, swapiRecebido)
	})

}
