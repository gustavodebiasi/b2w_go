package models

import (
	"gitlab.com/gustavodebiasi/b2w_go/database"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Swapi struct {
	Id               bson.ObjectId `bson:"_id"`
	Nome             string        `bson:"nome"`
	QuantidadeFilmes int32         `bson:"quantidade_filmes"`
}

type SwapiRepository struct {
	Database   database.MongoConnection
	collection *mgo.Collection
}

type ISwapiRepository interface {
	CriarOuAtualizar(Swapi) error
	BuscarPorNome(string) (Swapi, error)
}

func NovoSwapiRepository(d database.MongoConnection) *SwapiRepository {
	return &SwapiRepository{
		Database:   d,
		collection: d.Collection("swapi"),
	}
}

func (s *SwapiRepository) CriarOuAtualizar(swapi Swapi) error {
	filter := bson.M{"nome": swapi.Nome}
	update := bson.M{"$set": bson.M{"quantidade_filmes": swapi.QuantidadeFilmes}}
	_, erro := s.collection.Upsert(filter, &update)

	return erro
}

func (s *SwapiRepository) BuscarPorNome(nome string) (Swapi, error) {
	filter := bson.M{"nome": nome}

	var swapi Swapi
	erro := s.collection.Find(filter).One(&swapi)

	return swapi, erro
}
