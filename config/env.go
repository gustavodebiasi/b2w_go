package config

import (
	"log"
	"os"
	"regexp"

	"github.com/joho/godotenv"
)

const projectDirName = "b2w_go"

func CarregarEnv(testing bool) {
	re := regexp.MustCompile(`^(.*` + projectDirName + `)`)
	cwd, _ := os.Getwd()
	rootPath := string(re.Find([]byte(cwd)))
	var erro error

	erro = carregarEnvArquivo(rootPath, ".env")
	if erro != nil {
		if testing {
			erro = carregarEnvArquivo(rootPath, ".env.pipeline")
		} else {
			erro = carregarEnvArquivo(rootPath, ".env.default")
		}
	}

	if erro != nil {
		log.Fatal(erro.Error())

		os.Exit(-1)
	}
}

func carregarEnvArquivo(rootPath string, env string) error {
	var caminho string
	if rootPath != "" {
		caminho = rootPath + `/` + env
	} else {
		caminho = env
	}
	return godotenv.Load(caminho)
}
