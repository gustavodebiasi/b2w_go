package database

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gustavodebiasi/b2w_go/config"
	"gopkg.in/mgo.v2"
)

func TestConectar(t *testing.T) {
	config.CarregarEnv(true)
	var db = MongoConnection{}
	db.Conectar()

	var tipoEsperado *mgo.Database
	assert.IsType(t, tipoEsperado, db.Database)
}

func TestCollection(t *testing.T) {
	config.CarregarEnv(true)

	var db = MongoConnection{}
	db.Conectar()
	collection := db.Collection("test")

	var tipoEsperado *mgo.Collection
	assert.IsType(t, tipoEsperado, collection)
}
