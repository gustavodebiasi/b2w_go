package database

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/mgo.v2"
)

type MongoConnection struct {
	Database *mgo.Database
}

func (m *MongoConnection) Conectar() {
	databaseURL := fmt.Sprintf("mongodb://%s:%s", os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_PORT"))

	session, err := mgo.Dial(databaseURL)
	if err != nil {
		log.Fatal("Erro ao conectar na base de dados " + databaseURL)
	}

	m.Database = session.DB(os.Getenv("DATABASE"))
}

func (m *MongoConnection) Collection(collection string) *mgo.Collection {
	return m.Database.C(collection)
}
